import React from 'react';
import {
  StyleSheet,
  Text,
  View
  } from 'react-native';
  import Popularity from './Popularity'; 

  export default function VideoMetaRow({ likes, views }){
    return (
         <Popularity
         likes={likes}
         views={views}
          />
    );
}
