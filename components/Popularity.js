import React from 'react';
import {
  StyleSheet,
  Text,
  View
  } from 'react-native';

  export default function Popularity({ likes, views }){
    return (
      <View style={styles.popularity}>
       <Text style={styles.text}>{`${views} views`}</Text>
       <Text style={styles.text}>{`${likes} Likes`}</Text>
       </View>
    );
}

const styles = StyleSheet.create({
   popularity: {
     flexDirection: 'row',
     justifyContent: 'space-between'
   },
   text: {
     color: 'grey',
     fontStyle: 'italic',
     paddingVertical: 10,
     paddingHorizontal: 5
   }
})