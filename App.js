/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet
} from 'react-native';
import { StackNavigator } from 'react-navigation';
import Youtube from 'react-native-youtube';
import YoutubeVideoPlayer from './screens/YoutubePlayer';
import VideoList from './components/VideoList'

const apiKey ='AIzaSyAw5SyOK3rODFQHimTHLTrQlb3-SS9sB_4';
const channelId='UC3l8a3b0I41W3D9rrLPXRTg';
const results = 30;
const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  logo: {
    fontStyle: 'normal',
    paddingHorizontal: 10,
    fontWeight: 'bold',
    fontSize: 35,
  }
});

class App extends Component {
  static navigationOptions = {
    headerStyle : {
        backgroundColor: '#fff'
    },
    headerLeft:(
        <TouchableOpacity>
             <Text style={styles.logo}>Tiwa</Text>
        </TouchableOpacity>
  )
}

constructor(props){
  super(props);

  this.state = {
     data: []
  }
}

componentDidMount() {
  fetch(`https://www.googleapis.com/youtube/v3/search/?key=${apiKey}&channelId=${channelId}&part=snippet,id&order=date&maxResults=${results}`)
      .then( res => res.json())
      .then(res => {
          const videoId = [];
          res.items.forEach(item => {
              videoId.push(item);
          });

          this.setState({
              data: videoId
          });
      })
      .catch(error => {
          console.error(error);
      });
 }

render() {
    return (
      <View style={styles.container}>
       <VideoList {...this.props} items={this.state.data}/>
      </View>
    );
  }
}



const screens = StackNavigator({
  Home: {screen: App },
  YouTubeVideo: { screen: YoutubeVideoPlayer }
});

export default screens;
